Rust on Micros
===============


I don't know what my problem with Zinc was --- seems to work fine. I
think at the time I just didn't know enough about Rust etc... to
really understand what to do.

Basics are:

install the nightly rust

install toolchain. On arch you can:

    pacman -Sy arm-none-eabi-gcc arm-none-eabi-newlib

ln a target description to the example and do the cargo command. E.g.:

    cd examples/blink_k20
    ln -s ../../thumbv7em-none-eabi.json
    cargo build --target=thumbv7em-none-eabi --features mcu_k20 --release

    file target/thumbv7em-none-eabi/release/blink
    target/thumbv7em-none-eabi/release/blink: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), statically linked, not stripped

then objcopy to a binary:

    arm-none-eabi-objcopy -O binary ./target/thumbv7em-none-eabi/release/blink  blink.bin


For using Zinc as an external dependancy see:

https://github.com/posborne/zinc-example-lpc1768

Threading
----------

Zinc doesn't do threading b/c that's an OS / rust libstd thing (zinc only has libcore as that is the dependancy free core code).

Discussion re: Zinc and threading:

https://github.com/hackndev/zinc/issues/155



Old
====

see:

http://antoinealb.net/programming/2015/05/01/rust-on-arm-microcontroller.html
http://spin.atomicobject.com/2015/02/20/rust-language-c-embedded/
https://github.com/hackndev/zinc

building libcore
================

https://doc.rust-lang.org/core/

(N.B. libcore is UNSTABLE! the "correct" thing to use is the standard
library, but that has dependancies. core is the part without
dependancies...  other options might be to use this from
https://crates.io/crates/core-nightly/ (might not have the cortex-m0))

This won't work with the rust installed from pacman (doesn't have the commit hash).

Install rust nightly locally from source.
(from https://doc.rust-lang.org/book/nightly-rust.html)

    curl -s https://static.rust-lang.org/rustup.sh | sh -s -- --channel=nightly

(For some reason I don't understand, you can't actually build libcore
with the stable channel b/c of the #feature lines... this is some rust
thing I don't really understand. what I do know is you can't have any
#feature with the stable rustc...)

    rustc -v --version

get the commit hash = $HASH

    git clone git@github.com:rust-lang/rust.git
    cd rust
    git checkout $HASH
    cd ..
	
then, somewhere else do

    mkdir libcore-thumbv6

the following needs to run one directory up from where rust.git is cloned.

    wget https://raw.githubusercontent.com/hackndev/zinc/master/thumbv6-none-eabi.json
    rustc -C opt-level=2 -Z no-landing-pads --target thumbv6-none-eabi -g rust/src/libcore/lib.rs --out-dir libcore-thumbv6

That will build libcore.rlib in libcore-thumbv6

Installing the ARM crates
=========================

https://github.com/japaric/ruststrap/blob/master/1-how-to-cross-compile.md

https://www.reddit.com/r/rust/comments/2ut7qi/rust_how_good_is_the_support_of_cross_compilation/

rustc itself doesn't need to be built as a cross compiler. You can use
rustc --target with the name of any target Rust supports, or even a
custom target spec as a JSON file. But you need a copy of the stdlib
for that target, which is the point of building Rust itself with
./configure --target=...

these are in rust/mk/cfg.

can take

arm-unknown-linux-gnueabi.mk

and pacman -Sy arm-none-eabi-gcc

and make a arm-none-eabi-gcc.mk and change things to use that prefix.

which fails b/c

In file included from /usr/lib/gcc/arm-none-eabi/5.2.0/include-fixed/syslimits.h:7:0,
from /usr/lib/gcc/arm-none-eabi/5.2.0/include-fixed/limits.h:34,
from /home/malvira/repos/rust/rust/src/compiler-rt/lib/builtins/int_lib.h:67,
from /home/malvira/repos/rust/rust/src/compiler-rt/lib/builtins/absvdi2.c:15:
/usr/lib/gcc/arm-none-eabi/5.2.0/include-fixed/limits.h:168:61: fatal error: limits.h: No such file or directory

no limits. Which sets OS kinda limits:

http://pubs.opengroup.org/onlinepubs/009695399/basedefs/limits.h.html

