#!/bin/sh
rustc -C opt-level=2 -Z no-landing-pads --target thumbv6-none-eabi -g --emit obj -L ../libcore-thumbv6 -o hello.o examples/hello.rs
